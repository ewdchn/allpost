#######Installation#######
Required packages:
bsd-conv 7.5
https://github.com/buganini/bsdconv
installation: use install.sh, default installs to ~/.local



python-bsdconv 7.4
https://github.com/buganini/python-bsdconv
installation: 	$ python setup.py (default point bsd-conv library path to ~/.local)
				$ python setup.py [bsd-conv library path]

pyte
**The package included in modified and allpost will only work with this version
makesure other pyte packages are not installed on the system



#######Running#######
1.setup PYTHONPATH and LD_LIBRARY_PATH (ref: setupenv.sh) first

allpost.py:
python allpost.py [-v] [-o] [-d DATE] [-b BOARD]  [--debug] [--dump]
	|---- -v :verbose mode
	|---- -o : scan once then quit
	|---- -d : only scan designated date
	|---- -ds : show screen dump of connection
	|---- -b : only scan designated board
	|---- --debug: debug mode
	`---- --dump : print screen dump of connection on terminal
	
example usage:
By default allpost.py performs a sweep ALLPOST regularly and filter out articles from set of specific boards starting with today
--silent run:
	$python allpost.py
--with output of status detail:
	$python allpost.py -v
--turn on debuggingn options, realtime data floods the terminal
	$python allpost.py --debug
--starts from specific date (< today)(e.g. 11/15)
	$python allpost.py -d 1115
--Instead sweeping allpost, only monitors articles from a specific board
	$python allpost.py -b NTU
--Sweep only once and quit
	$python allpost.py -o
--For advanced debugging option, --dump dump the raw data from connection to the terminal, unless for advaced debugging purposes, turning this flag on is not recommended as the raw data would flood the terminal and masking all other messages
	
monitor.py:
python monitor.py [-v] [--debug] [--dump]
see allpost.py for option details
example usages:
--all flags are optional for extra output, by default simply run the monitor
	$python monitor.py


mail.py
python mail.py [-v] [-u user] [-t subject] [-m mail content]
example usages:
-- send mail to atoniea, with subject "testSub" and content"test"
	$python mail.py -u atoniea -t testSub -m test