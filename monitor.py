# -*- coding: utf-8 -*-


import time
import allpost
import re
import sys
import datetime
import argparse
import os
import telnetlib

MONITORLIST = set(['cohabitation',
                   'marriage',
                   'GetMarry',
                   'CCRomance',
                   'first-wife',
                   'JD_Lover',
                   'multi-lovers',
                   'third-person',
                   'Boy-Girl',
                   'CATCH',
                   'DistantLove',
                   'esahc',
                   'love',
                   'chatskill',
                   'MenTalk',
                   'WomenTalk',
                   'feminine_sex',
                   'sex',
                   'Aquarius',
                   'Aries',
                   'Cancer',
                   'Capricornus',
                   'Gemini',
                   'Leo',
                   'Libra',
                   'Pisces',
                   'Sagittarius',
                   'Scorpio',
                   'Taurus',
                   'Virgo'])
OUTPUTDIR = "monitor"


def today():
    return int(datetime.date.today().strftime("%Y%m%d"))


def writeLog(_board, _data):
    with open("{0}/{1}_{2}.txt".format(OUTPUTDIR, today(), _board), "a") as myfile:
        myfile.write(_data + '\n')


def main():
    for targetBoard in MONITORLIST:
        buf = allpost.goToBoard(targetBoard, _getStartEntries=True)
        if DEBUG:
            print buf
        buf = buf.split("\n")
        l = buf[2]
        if DEBUG:
            print '[' + l + ']'
        m = re.match('^   編號    日 期 作  者       文  章  標  題 *人氣:(\d+) *$', l)
        popularity = int(m.group(1))
        log_data = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S') + '--' + str(popularity)
        if VERBOSE:
            print '{0}: '.format(targetBoard) + log_data
        writeLog(targetBoard, log_data)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', action="store_true", dest="v", default=False) #verbose
    parser.add_argument('--debug', action="store_true", dest="d", default=False) #DEBUG option
    parser.add_argument('--dump', action="store_true", dest="ds", default=False) #DUMPSCREEN option

    args = parser.parse_args(sys.argv[1:])

    VERBOSE = args.v or args.d
    allpost.VERBOSE = args.v
    allpost.DEBUG = args.d
    DEBUG = args.d
    allpost.DUMPSCREEN = args.ds

    #make output dir
    if not os.path.exists(OUTPUTDIR):
        os.mkdir(OUTPUTDIR)
    try:
        allpost.login(_killOtherConn=True,_user="stevec",_passwd="87654321")
    except:
        print "login failed"
        sys.exit(0)


    #first sweep
    lastSweep=0
    try:
        main()
        lastSweep = time.time()
    except:
        allpost.conn = telnetlib.Telnet("ptt.cc")
        allpost.login(_killOtherConn=True,_user="stevec",_passwd="87654321")
    while True:
        if time.time() - lastSweep >= 60 * 2:
            if VERBOSE:
                print 'update'
            try:
                main()
            except:
                time.sleep(300)
                allpost.conn = telnetlib.Telnet("ptt.cc")
                allpost.login(_killOtherConn=True, _user="stevec", _passwd="87654321")
                continue
            lastSweep = time.time()
        else:
            time.sleep(10)
            allpost.term_comm()