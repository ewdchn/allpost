# -*- coding: utf-8 -*-
import os
import sys
import re
import time
import argparse
import bsdconv
import telnetlib
import time
import inspect
import datetime
import pyte
import socket
import traceback
from inspect import currentframe, getframeinfo

#CONSTANTS
KEYBOARD_RIGHT = "\x1b[C"
KEYBOARD_UP = "\x1b[A"
KEYBOARD_DOWN = "\x1b[B"
KEYBOARD_LEFT = "\x1b[D"
KEYBORAD_RIGHT = "\x1b[C"
KEYBOARD_PAGEUP = "\x1b[5~"
KEYBOARD_PAGEDOWN = "\x1b[6~"
KEYBOARD_CTRL_L = '\x0C'
KEYBOARD_HOME = "\x1bOH"
KEYBOARD_END = "\x1bOF"
KEYBOARD_ENTER = "\x1bOM"
LASTBUF = None
FILTERLIST = {'cohabitation', 'marriage', 'GetMarry', 'CCRomance', 'first-wife', 'JD_Lover', 'multi-lovers',
              'third-person', 'Boy-Girl', 'CATCH', 'DistantLove', 'esahc', 'love', 'chatskill', 'MenTalk', 'WomenTalk',
              'feminine_sex', 'sex', 'Aquarius', 'Aries', 'Cancer', 'Capricornus', 'Gemini', 'Leo', 'Libra', 'Pisces',
              'Sagittarius', 'Scorpio', 'Taurus', 'Virgo'}

cwd = os.getcwd()

#big5(含uao)，去雙色字，轉utf-8
Gconv = bsdconv.Bsdconv("ansi-control,byte:big5-defrag:byte,ansi-control|skip,big5:utf-8,bsdconv_raw")
Gconv.init()

stream = pyte.Stream()
screen = pyte.Screen(80, 24)
screen.mode.discard(pyte.modes.LNM)
stream.attach(screen)
conn = telnetlib.Telnet("ptt.cc")

#ptt can't display 6-digit article serial
def getpnum(_pnumStr):
    num = int(_pnumStr)
    if len(_pnumStr)==5 and _pnumStr[0] =="0":
        num+=100000
    return num


#date of today in format mmdd
def getDateFormat(_offset=0):
    outputDate = datetime.date.today()+datetime.timedelta(_offset)
    return int(outputDate.strftime("%m%d"))


#error handling
def error(_message=""):
    global GtargetBoard, GtargetDate,LASTBUF,VERBOSE,DEBUG
    frame,filename,line_number,function_name,lines,index=\
        inspect.getouterframes(inspect.currentframe())[1]
    if DEBUG:
        print "ERROR: "+frame
        print LASTBUF
        print _message
    raise


#write to connection, return screen content
def term_comm(w=None, wait=None, waitTime=0.1):
    global LASTBUF,VERBOSE,DEBUG,conn
    tryCnt = 0
    s=None
    ret = None
    #write to connection
    try:
        if w == None:
            conn.write(KEYBOARD_CTRL_L) # Ctrl+L for redraw
        else:
            conn.write(w)
    except socket.error:
        error()

    #wait
    if wait != False:
        while s==None or ret == None:
            tryCnt += 1
            if tryCnt > 100:
                error("term_comm tryCnt out")
            try:
                time.sleep(waitTime)
                s = conn.read_very_eager()
                s = Gconv.conv_chunk(s)
                stream.feed(s.decode("utf-8"))
                break
            except socket.error:
                if DEBUG:
                    print str(getframeinfo(currentframe()).lineno)+sys._getframe().f_code.co_name+": "+ str(sys.exc_traceback.tb_lineno)
                error()
            except IndexError:
                if DEBUG:
                    print str(getframeinfo(currentframe()).lineno)+sys._getframe().f_code.co_name+": "+ str(sys.exc_traceback.tb_lineno)
                continue
            except EOFError:
                if DEBUG:
                    print str(getframeinfo(currentframe()).lineno)+sys._getframe().f_code.co_name+": "+ str(sys.exc_traceback.tb_lineno)
                error()
            except Exception as e:
                if DEBUG:
                    print str(getframeinfo(currentframe()).lineno)+ sys._getframe().f_code.co_name+": "+ str(sys.exc_traceback.tb_lineno)
                error()

    #ptt tries to disconnect
    ret = "\n".join(screen.display).encode("utf-8")
    scr = ret.split('\n')
    if scr[-3].find('程式耗用過多計算資源, 立刻斷線。') != -1 and scr[-1].find('可能是 (a)執行太多耗用資源的動作 或 (b)程式掉入無窮迴圈. 請洽 PttBug 板') != -1:
        error("disconnect notification received")

    if DUMPSCREEN and w == None and LASTBUF != ret:
        print LASTBUF
    LASTBUF = ret
    return ret


#Goto Target Board, return startSerial,EndSerial
#Screen change: Main Screen/article List -> article List (of designated Board)
def goToBoard(_targetBoard='ALLPOST', _date=None, _getStartEntries=False):
    global VERBOSE,DEBUG
    if VERBOSE:
        print 'goto board [{0}], target Date {1}'.format(_targetBoard, _date)
    imin = 1
    imax = 10

    #go into board
    buf = term_comm("s")
    buf = term_comm(_targetBoard + "\r")
    time.sleep(3) #Must sleep here, else can't get inside board
    buf = term_comm()

    #---------進板畫面------------------
    if buf.find("請按任意鍵繼續") != -1:
        buf = term_comm(" ")

    #directives: Just go to board, don't grab entry counts, for popularity monitoring
    if _getStartEntries:
        return buf

    #grab entry counts
    term_comm(KEYBOARD_HOME, wait=False)
    term_comm(KEYBOARD_END, wait=True)
    ppdate, pnum, pdate, board = focusArticleInfo(_targetBoard)
    imax = pnum
    #尋找符合目標日期的第一筆
#    if _date == None:
#        return imin, imax
    if VERBOSE:
        sys.stdout.write('finding where to start...')

    #For ALLPOST
    if _targetBoard == 'ALLPOST':
        while imax >= imin:
            if VERBOSE:
                sys.stdout.write('.')
            imid = (imin + imax) / 2
            term_comm("%d\r" % imid, wait=True) #send article number
            ppdate, pnum, pdate, board = focusArticleInfo(_targetBoard)

            if pdate < _date:
                imin = imid + 1
            elif pdate > _date or ppdate == _date:
                imax = imid - 1
            else:
                break
                #print imid,imax

    #For other boards
    else:
        while ppdate >= _date:
            term_comm(KEYBOARD_PAGEUP + KEYBOARD_DOWN)
            ppdate, pnum, pdate, board = focusArticleInfo(_targetBoard)
        imid = pnum

    if VERBOSE:
        sys.stdout.write('...done\n')

    return imid, imax


#perform login action, goto main Screen
def login(_killOtherConn=False, _user=None, _passwd=None):
    global VERBOSE,DEBUG
    tryCnt = 0
    user = _user or "atoniea"
    passwd = _passwd or "lovetony"
    if VERBOSE:
        sys.stdout.write("login...")
    if DEBUG:
        sys.stdout.write( "welcome screen...")
    if DEBUG:
        sys.stdout.write("username...")
    while True:
        tryCnt += 1
        s = term_comm()
        if s.find("或以 new 註冊:") != -1:
            term_comm(user + "\r")
            break
        if tryCnt > 3:
            print s
            raise
        time.sleep(3)
    if DEBUG:
        sys.stdout.write("password...")
    while True:
        s = term_comm()
        if s.find("請輸入您的密碼:") != -1:
            term_comm(passwd + "\r",waitTime=5)
            break
    while True:
        s = term_comm()
        if s.find("按任意鍵繼續") != -1:
            term_comm(" ",waitTime=1)
            break
        elif s.find("登入中，請稍候") != -1 or s.find("需時較久...") != -1:
            time.sleep(3)
        elif s.find("您要刪除以上錯誤嘗試的記錄嗎? [y/N]") != -1:
            term_comm("y\r",waitTime=10)
        elif s.find("您想刪除其他重複登入的連線嗎？[Y/n]") != -1:
            if _killOtherConn:
                term_comm("y\r",waitTime=10)
            else:
                term_comm("n\r",waitTime=10)
        else:
            raise
    if VERBOSE:
        sys.stdout.write("done\n")






# return boradName, date, serial number of selected article
# The article list layout of ALLPOST is different from usual BOARDS
#the wrapper
def focusArticleInfo(_targetBoard="ALLPOST"):
    global VERBOSE,DEBUG
    if _targetBoard == "ALLPOST":
        ppdate, pnum, pdate, board = ALLPOST_focusArticleInfo()
    else:
        ppdate, pnum, pdate, board = BOARD_focusArticleInfo() + (_targetBoard,)
    if DEBUG:
        print "focusArticleInfo: ", ppdate, pnum, pdate, board
    return ppdate, pnum, pdate, board

#for usual boards
def BOARD_focusArticleInfo():
    global VERBOSE,DEBUG
    trial = 0
    a = term_comm(wait=False).split("\n")
    while True:
        try:
            for i, l in enumerate(a):
                if l.startswith("●"):
                    #pinned articles, return the last (latest article) unpinned
                    if re.search("^● *★.*", l):
                        j = i - 1
                        while re.search('^ *★.*', a[j]):
                            j -= 1
                        m = re.search("^ *(\d+).+?(\d{1,2})/(\d+).*$", a[j - 1])
                        pnum = getpnum(m.group(1))
                        ppdate = int(m.group(2)) * 100 + int(m.group(3))
                        m = re.match("^ *(\d+).+?(\d{1,2})/(\d+).*$", a[j])
                        pdate = int(m.group(2)) * 100 + int(m.group(3))
                        return ppdate, pnum, pdate
                    #case: focus is on top of the list
                    elif (a[i - 1].startswith("   編號    日 期 作  者       文  章  標  題")):
                        b = term_comm(KEYBOARD_DOWN).split("\n")
                        term_comm(KEYBOARD_UP)
                        m = re.match("^ *(\d+).+?(\d{1,2})/(\d+).*$", b[3])
                        pnum = 1  #so NextArticle() would continue sweep
                        ppdate = int(m.group(2)) * 100 + int(m.group(3)) #so goToboard would continue sweep
                    #usual cases
                    else:
                        m = re.match("^ *(\d+).+?(\d{1,2})/(\d+).*$", a[i - 1])
                        pnum = getpnum(m.group(1))
                        ppdate = int(m.group(2)) * 100 + int(m.group(3))
                    m = re.match("^● *\d+.+?(\d{1,2})/(\d+).*$", l)
                    pdate = int(m.group(1)) * 100 + int(m.group(2))
                    return ppdate, pnum, pdate


        except:
            if VERBOSE:
                print '!'
            time.sleep(0.1)
            a = term_comm(KEYBOARD_CTRL_L).split("\n")
            trial += 1
            if trial > 20:
                error("BOARD_focusArticleInfo error")

#for ALLPOST
def ALLPOST_focusArticleInfo():
    trial = 0
    a = term_comm().split("\n")
    while True:
        try:
            for i, l in enumerate(a):
                if l.startswith("●"):
                    if (a[i - 1].startswith("   編號    日 期 作  者       文  章  標  題")):
                        pnum = 1
                        ppdate = 0
                    else:
                        m = re.match("^ *(\d+)[ ~+]+(\d+)/(\d+).*$", a[i - 1])
                        pnum = getpnum(m.group(1))
                        ppdate = int(m.group(2)) * 100 + int(m.group(3))
                    m = re.match("^● *\d+[ ~+]+(\d+)/(\d+) *.*\.(\S*)板", l)
                    pdate = int(m.group(1)) * 100 + int(m.group(2))
                    board = m.group(3)
                    return ppdate, pnum, pdate, board
            raise IOError
        except:
            a = term_comm().split("\n")
            trial += 1
            if trial > 100:
                error("ALLPOST_focusArticleInfo Error")




#write file to {_board}/{_AID}.txt
def writeFile(_articleBuf, _articleBoard='ALLPOST', _AID=None):
    if _AID == None:
        error("no AID")
    if not os.path.exists('output/{0}'.format(_articleBoard)):
        os.mkdir('output/{0}'.format(_articleBoard))
    title = _articleBuf[1].rstrip(' ')
    #get title
    m = re.match('^ *標題 *(Re:)?(Fw:)? *(.+)', title)
    if m:
        title = m.group(3)
        title = title.replace('/', '')
        fName = "output/{0}/{1}__{2}.txt".format(_articleBoard, title, _AID)
        if m.group(1) != None:
            fName = "output/{0}/Re_{1}__{2}.txt".format(_articleBoard, title, _AID)

    else:
        return

    #write thread log
    try:
        with open('output/series_' + title, 'r') as f:
            if fName in f.read().splitlines():
                pass
            else:
                raise
    except:
        with open('output/series_' + title, 'a') as f:
            f.write(fName + '\n')

    #write article File
    articleBuf = [x.rstrip() for x in _articleBuf]
    while not articleBuf[-1]:
        articleBuf = articleBuf[:-1]
    try:
        f = open(fName, "w")
        try:
            f.write("\n".join(articleBuf))
            f.write("\n")
        except:
            print "writeFile: ", "ERROR writing {0}".format(fName)
        f.close()
    except:
        print "writeFile: ", "ERROR opening {0}".format(fName)




#check if next article exists
#keyboard send : arcitleSerial + \r
#screen Change: none
def nextArticle():
    global GtargetDate, GcurArticleSerial, GlastArticleSerial, GtargetBoard
    #enter article number, get info, press right arrow
    while True:
        GcurArticleSerial += 1
        buf = term_comm('{0}'.format(GcurArticleSerial) + '\r')# article number
        ppdate, pnum, pdate, board = focusArticleInfo(GtargetBoard)
        #check if it is the last article with Date and serial
        if pdate > GtargetDate or GlastArticleSerial == pnum:
            if not ALL:
                return None, None, None, None


        #find the next not deleted article
        for i, l in enumerate(buf.split('\n')):
            if (l.startswith("●") and l.find("本文已被刪除") != -1) or (l.startswith("●") and l.find("-            □") != -1):
                break
        #------------for else loop, following block is executed if break before this line is not executed
        else:
            break

        #------------for else loop--------------------
        continue
    GlastArticleSerial = pnum
    return ppdate, pnum, pdate, board


#function called when reading one article is complete
#   |---- call writeFile Function to save article
#   `---- call nextArticle Function to check if next article exists
#   keyboard send: Q -> LEFT
#   screen change: article content -> article list
def finishReading(_articleBuf, _articleBoard='ALLPOST'):
    trial = 0
    while True:
        trial += 1
        s = term_comm('Q')  #Q to see AID
        a = s.split('\n')
        aidMatch = re.match('^.*文章代碼\(AID\): *#(\S+) *.*$', a[-5])
        if aidMatch:
            break
        if trial > 10:
            error("AID Not Found")
    AID = aidMatch.group(1)
    buf = term_comm(KEYBOARD_LEFT)  #left
    writeFile(_articleBuf, _articleBoard, AID)
    return nextArticle()


def main():
    global GtargetDate, GstartSerial, GendSerial, GcurArticleSerial, GlastArticleSerial, Gconv, GtargetBoard, FILTERLIST


    #parse parameters
    if DEBUG:
        print 'list: ' + ','.join(FILTERLIST)


    #get article serial to start with
    GstartSerial, GendSerial = goToBoard(GtargetBoard, GtargetDate)
    if STARTSERIAL != 0:
        GstartSerial = int(STARTSERIAL)
    if VERBOSE:
        print GstartSerial, GendSerial
    GlastArticleSerial = -1
    GcurArticleSerial = GstartSerial - 1



    #goto First article to grab
    ppdate, pnum, pdate, board = nextArticle()
    while GtargetBoard == 'ALLPOST':
        if (board in FILTERLIST) or (ppdate == None):
            break
        else:
            ppdate, pnum, pdate, board = nextArticle()

    if ppdate == None:
        if VERBOSE:
            print 'No matching article for designated date, done'
        return
    else:
        term_comm(KEYBOARD_RIGHT)

    #variable initialization
    no = 0
    trial = 0
    flag = False
    lastTail = 0
    articleBuf = []

    if VERBOSE:
        print "starting with ", GcurArticleSerial

    while True:
        s = term_comm()
        screenBuf = s.split("\n")
        m = re.match("^.*?目前顯示: 第 *(\d+)~(\d+) *行.*$", screenBuf[-1], re.U)
        #-----------------------handle unusual articles--------------------------
        if not m:
            if s.find("◆ 此文章無內容") != -1:
                term_comm("\r")
                if DEBUG:
                        print "No Content"
                while True:
                    ppdate, pnum, pdate, board = nextArticle()
                    if GtargetBoard == 'ALLPOST':
                        if (board in FILTERLIST ) or (ppdate == None):
                            break
                        else:
                            continue
                    else:
                        break
                term_comm(KEYBOARD_RIGHT)
                continue
            elif screenBuf[-1].find("★ 這份文件是可播放的文字動畫，要開始播放嗎？ [Y/n]") != -1:
                trial = 0
                term_comm("n", wait=True)
                continue
            elif screenBuf[-1].find("▲此頁內容會依閱讀者不同,原文未必有您的資料") != -1:
                a = s.split("\n")
                ll = a[-1]
                a = a[:-1]
                for l in a:
                    articleBuf.append(l)
                while ll.find("(100%)") == -1: #middle of article
                    s = term_comm(KEYBOARD_DOWN, wait=True) #down
                    a = s.split("\n")
                    ll = a[-1]
                    a = a[:-1]
                    articleBuf.append(a[-1])
                    #no continue here, control flow will go to check position in article
            else:
                trial += 1
                if trial > 100:
                    error()
                continue

        # --------------------------handle normal articles------------------------------
        else:
            ll = screenBuf[-1]
            screenBuf = screenBuf[:-1]
            nowHead = int(m.group(1))
            nowTail = int(m.group(2))
            if lastTail != nowTail:
                trial = 0
            else:
                flag = True
                trial += 1
                if trial > 50:
                    error("Failed to Read Article")
                else:
                    #term_comm("\x1b[5~",wait=True) #send Page Up
                    term_comm(KEYBOARD_PAGEDOWN, wait=True) #send page down
                continue
            screenBuf = screenBuf[lastTail - nowHead + 1:]
            lastTail = nowTail
            for l in screenBuf:
                articleBuf.append(l)

        #-------------------------------------Find position in article----------------------------#
        if ll.find("(100%)") == -1: #middle of article
            term_comm(KEYBOARD_PAGEDOWN, wait=True) #send page down

        #end of article
        else:
            lastTail = 0
            no += 1
            if flag and VERBOSE:
                flag = False
                print articleBuf
            elif (no % 100 == 0) and VERBOSE:
                sys.stdout.write('.')
            ppdate, pnum, pdate, board = finishReading(articleBuf, board)
            articleBuf = []
            while GtargetBoard == 'ALLPOST':
                if (board in FILTERLIST) or (ppdate == None):
                    break
                else:
                    ppdate, pnum, pdate, board = nextArticle()
            if ppdate == None:
                break
            else:
                term_comm(KEYBOARD_RIGHT, wait=True)     #right

    #-------------------------sweep complete------------------------------------------

    #go back to main screen
    term_comm(KEYBOARD_LEFT + KEYBOARD_LEFT + KEYBOARD_LEFT + KEYBOARD_LEFT + KEYBOARD_LEFT)

    if VERBOSE:
        sys.stdout.write("\ndone. waiting...")
    return


def reconnect():
    global VERBOSE,DEBUG,conn,Gconv,stream,screen
    fallbackTime = 600
    if VERBOSE:
        print str(getframeinfo(currentframe()).lineno) + ": " + "Connection close by remote, reconnecting in {0} secs...".format(fallbackTime)
        print datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
    while True:
        try:
            time.sleep(fallbackTime)
            Gconv = bsdconv.Bsdconv("ansi-control,byte:big5-defrag:byte,ansi-control|skip,big5:utf-8,bsdconv_raw")
            Gconv.init()
            stream = pyte.Stream()
            screen = pyte.Screen(80, 24)
            screen.mode.discard(pyte.modes.LNM)
            stream.attach(screen)
            conn = telnetlib.Telnet("ptt.cc")
            login(_killOtherConn=True)
            if VERBOSE:
                print "reconnect Complete"
            time.sleep(10)
            if DEBUG:
                print term_comm()
            break
        except:
           fallbackTime *=2
           print "reconnect failed, retrying in {0} secs....".format(fallbackTime)
           continue
    print "reconnection complete"
#-----------------------------------main-------------------------------------------------
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', action="store", dest="targetDate", type=int,
                        default=getDateFormat())
    parser.add_argument('-b', action="store", dest="targetBoard", default='ALLPOST')
    parser.add_argument('-s', action="store", dest="startSerial", type=int, default=0)
    parser.add_argument('-v', action="store_true", dest="v", default=False) #verbose
    parser.add_argument('-o', action="store_true", dest="o", default=False) #once only
    parser.add_argument('-a', action="store_true", dest="a", default=False) #ALL articles,ignore date
    #parser.add_argument('-k', action="store_true", dest="k", default=False) #kill other connections
    parser.add_argument('--debug', action="store_true", dest="d", default=False) #debug output
    parser.add_argument('--dump', action="store_true", dest="ds", default=False) #dump screen to stdout



    #parse Args
    args = parser.parse_args(sys.argv[1:])
    GtargetBoard = args.targetBoard
    GtargetDate = args.targetDate
    VERBOSE = args.v or args.d
    DEBUG = args.d
    ALL = args.a
    DUMPSCREEN = args.ds
    STARTSERIAL = args.startSerial
    #KILLOTHERCONNECTION = args.k
    ONCEONLY = args.o


    #make output dir
    if not os.path.exists("output"):
        os.mkdir("output")

    #login, goto designated board
    login(_killOtherConn=True)
    lastSweepTimeStamp = time.time() - 1200

    #after first sweep, change startdate to today - 2
    while True:
        #swep timer
        if time.time() - lastSweepTimeStamp >= 600:
            print "\nsweep" + datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
            dateArr = [getDateFormat(-2), getDateFormat(-1), getDateFormat()]
            while dateArr[0] > dateArr[-1]:
                dateArr = dateArr[1:]
            for GtargetDate in dateArr:
                try:
                    main()
                    if ONCEONLY:
                        sys.exit(0)
                except:
                    reconnect()
                    #reset sweep time stamp after reconnection
                    lastSweepTimeStamp = time.time()
        #sweep timer not yet
        else:
            try:
                time.sleep(60)
                term_comm()
                if VERBOSE:
                    sys.stdout.write(".")
            except:
                reconnect()
                lastSweepTimeStamp = time.time()
