__author__ = 'ewdchn'
import allpost
import argparse

'''Send Mail to other users, start from Main Screen'''
def mail(_mailReceipent,_mailSubject='',_mailContent=''):
    allpost.term_comm('M\rS\r')
    screen = allpost.term_comm('{0}\r'.format(_mailReceipent)).split('\n')
    if screen[0].find('電子郵件')!=-1:
        print "user not found"
        return
    allpost.term_comm('{0}\r'.format(_mailSubject))
    allpost.term_comm(_mailContent)
    allpost.term_comm("\x18") #Ctrl+X
    allpost.term_comm("s\r") #儲存
    allpost.term_comm("Y\r") #已順利寄出，是否自存底稿
    allpost.term_comm('\r')  #請按任意鍵繼續




if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', action="store_true", dest="v", default=False) #verbose
    parser.add_argument('--debug', action="store_true", dest="d", default=False) #DEBUG option
    parser.add_argument('--dump', action="store_true", dest="ds", default=False) #DUMPSCREEN option
    parser.add_argument('-u', action="store", dest="user",required=True)
    parser.add_argument('-t', action="store",dest="title",required=True)
    parser.add_argument('-m', action="store",dest="content",required=True)

    args = parser.parse_args(sys.argv[1:])


    allpost.DUMPSCREEN=args.ds
    allpost.DEBUG = args.d
    DEBUG = args.d

    allpost.login()
    mail(args.user,args.title,args.content)