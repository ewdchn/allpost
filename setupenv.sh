#!/bin/bash
set -x
export PYTHONPATH=$HOME/.local/lib/python2.7/site-packages
export LD_LIBRARY_PATH=$HOME/.local/lib:$LD_LIBRARY_PATH
